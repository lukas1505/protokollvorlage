#!/usr/bin/env python3

"""
  Created: 1/18/22
  by: Lukas Schuettler

  Standalone script to pull private signs
"""

import argparse
import os
import requests

_assets = os.path.join('src', 'assets', 'privat')

parser = argparse.ArgumentParser()
parser.add_argument("token", help="Token for private images")
args = parser.parse_args()

tokens = args.token.split('#')

sign_ls = requests.get(
    f'https://firebasestorage.googleapis.com/v0/b/lufixsch-cdn.appspot.com/o/unterschriften%2Fsign_ls.png?alt=media&token={tokens[0]}').content
with open(os.path.join(_assets, 'sign_ls.png'), 'wb') as handler:
    handler.write(sign_ls)

sign_ts = requests.get(
    f'https://firebasestorage.googleapis.com/v0/b/lufixsch-cdn.appspot.com/o/unterschriften%2Fsign_ts.png?alt=media&token={tokens[1]}').content
with open(os.path.join(_assets, 'sign_ts.png'), 'wb') as handler:
    handler.write(sign_ts)